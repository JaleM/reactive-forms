import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpClient : HttpClient) { }

  public getUsers(): Observable<any> {
    return this.httpClient.get(`http://localhost:3500/users`);
  }

  public sendUsers(val): Observable<any> {
    return this.httpClient.post(`http://localhost:3500/users`, val);
  }

}