
import { FormGroup, Validators } from '@angular/forms';


export function checkIfMatchingEmail(email: string, confirmEmail: string) {
    return (group: FormGroup) => {
      let emailInput = group.controls[email],
          emailConfirmationInput = group.controls[confirmEmail];
      if (emailInput.value !== emailConfirmationInput.value) {
        return emailConfirmationInput.setErrors({mustMatch: true});
      }
      
    }
  }


  