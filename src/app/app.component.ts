import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from './user.service';
import { checkIfMatchingEmail } from './validator';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  signUpForm: FormGroup;
  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    private httpClient: HttpClient
  ) { }

  formControlValueChanged() {
    const phoneControl = this.signUpForm.get('phone');
    this.signUpForm.get('check').valueChanges.subscribe(
      (mode: string) => {
        console.log(mode);
        if (mode === 'text') {
          phoneControl.setValidators([Validators.required]);
        }
        else if (mode === 'email') {
          phoneControl.clearValidators();
        }
        phoneControl.updateValueAndValidity();
      });

  }


  ngOnInit() {
    this.formSign();
    this.formControlValueChanged();
  }


  formSign(): void {
    this.signUpForm = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      confirmEmail: ['', [Validators.required]],
      phone: [''],
      rating: [''],
      check: ['email'],
      sendCatalog: true

    },
      { validator: checkIfMatchingEmail('email', 'confirmEmail') }
    );

  }


  get name() { return this.signUpForm.get('firstName'); }
  get lastName() { return this.signUpForm.get('lastName'); }
  get email() { return this.signUpForm.get('email'); }
  get confirmEmail() { return this.signUpForm.get('confirmEmail'); }
  get phone() { return this.signUpForm.get('phone'); }
  get check() { return this.signUpForm.get('check'); }


  onSubmit() {
    this.userService.sendUsers(this.signUpForm.value).subscribe(res =>
      console.log(res));
     }




}